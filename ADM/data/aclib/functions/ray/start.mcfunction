#> aclib:ray/start
# @context any - 射线开始的位置和方向
# @input score
#   *aclib_var_raymode* - 射线检测模式
# 在指定实体的位置和朝向释放一个射线
tag @s add aclib_var_self
function aclib:ray/ray