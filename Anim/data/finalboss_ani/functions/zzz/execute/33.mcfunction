#Project generated by Animated Java (https://animated-java.dev/)

execute if entity @s[tag=aj.finalboss_ani.anim.animation.finalboss2.ray] at @s run function finalboss_ani:animations/animation.finalboss2.ray/next_frame
execute if entity @s[tag=aj.finalboss_ani.anim.animation.finalboss2.up] at @s run function finalboss_ani:animations/animation.finalboss2.up/next_frame
execute if entity @s[tag=aj.finalboss_ani.anim.animation.finalboss2.down] at @s run function finalboss_ani:animations/animation.finalboss2.down/next_frame
execute if entity @s[tag=aj.finalboss_ani.anim.animation.finalboss2.handup] at @s run function finalboss_ani:animations/animation.finalboss2.handup/next_frame
execute if entity @s[tag=aj.finalboss_ani.anim.animation.finalboss2.handdown] at @s run function finalboss_ani:animations/animation.finalboss2.handdown/next_frame
execute if entity @s[tag=aj.finalboss_ani.anim.animation.finalboss2.shield_rotate] at @s run function finalboss_ani:animations/animation.finalboss2.shield_rotate/next_frame
scoreboard players operation @s aj.finalboss_ani.animating = .aj.animation aj.finalboss_ani.animating