#Project generated by Animated Java (https://animated-java.dev/)

execute if score .this aj.frame matches 8 run data modify entity @s Pose.Head set value [0f,-11.5f,0f]
execute if score .this aj.frame matches 9 run data modify entity @s Pose.Head set value [0f,-7f,0f]
execute if score .this aj.frame matches 10 run data modify entity @s Pose.Head set value [0f,-2.5f,0f]
execute if score .this aj.frame matches 11 run data modify entity @s Pose.Head set value [0f,2f,0f]
execute if score .this aj.frame matches 12 run data modify entity @s Pose.Head set value [0f,6.5f,0f]
execute if score .this aj.frame matches 13 run data modify entity @s Pose.Head set value [0f,11f,0f]
execute if score .this aj.frame matches 14 run data modify entity @s Pose.Head set value [0f,15.5f,0f]
execute if score .this aj.frame matches 15 run data modify entity @s Pose.Head set value [0f,20f,0f]