scoreboard players set @s fishsoup_item 0
scoreboard players operation @s[nbt={SelectedItem:{tag:{id:"fishsoup:fish_soup"}}}] fishsoup_item = #FS_C_FISHSOUP fishsoup_item
scoreboard players operation @s[nbt={SelectedItem:{tag:{id:"fishsoup:memory_soup"}}}] fishsoup_item = #FS_C_MEMORYSOUP fishsoup_item
scoreboard players operation @s[nbt={SelectedItem:{tag:{id:"fishsoup:peach"}}}] fishsoup_item = #FS_C_PEACH fishsoup_item