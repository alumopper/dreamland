execute if score dream_tower_generation dream_counter matches 2 run place template dreamland:dream_tower/dream_castle.floor -18 120 -18
execute if score dream_tower_generation dream_counter matches 4 run place template dreamland:dream_tower/dream_castle.second1 -14 141 -14
execute if score dream_tower_generation dream_counter matches 6 run place template dreamland:dream_tower/dream_castle.second2 -14 153 -14
execute if score dream_tower_generation dream_counter matches 8 run place template dreamland:dream_tower/dream_castle.second3 -14 165 -14
execute if score dream_tower_generation dream_counter matches 10 run place template dreamland:dream_tower/dream_castle.second4 -14 177 -14
execute if score dream_tower_generation dream_counter matches 12 run place template dreamland:dream_tower/dream_castle.second5 -14 189 -14
execute if score dream_tower_generation dream_counter matches 14 run place template dreamland:dream_tower/dream_castle.second6 -14 201 -14
execute if score dream_tower_generation dream_counter matches 16 run place template dreamland:dream_tower/dream_castle.second7 -14 213 -14
execute if score dream_tower_generation dream_counter matches 18 run place template dreamland:dream_tower/dream_castle.top1 -14 225 -14
execute if score dream_tower_generation dream_counter matches 20 run place template dreamland:dream_tower/dream_castle.top2 -14 237 -14
execute if score dream_tower_generation dream_counter matches 22 run place template dreamland:dream_tower/dream_castle.side1 -15 203 -15 clockwise_90
execute if score dream_tower_generation dream_counter matches 24 run place template dreamland:dream_tower/dream_castle.side2 15 203 -15 180
execute if score dream_tower_generation dream_counter matches 26 run place template dreamland:dream_tower/dream_castle.side3 15 203 15 counterclockwise_90
execute if score dream_tower_generation dream_counter matches 28 run place template dreamland:dream_tower/dream_castle.side4 -15 203 15